<?php
function leer_data(){
	$_fp = fopen("php://stdin", "r");
	$archivo=fread($_fp, 1000000);
	$lineas= explode("\n", $archivo);
	$cadenas= explode(" ", $lineas[0]);
	$n=(int)$cadenas[0];
	$m=(int)$cadenas[1];
	$k=(int)$cadenas[2];
	$pescados=array();
	$l=1;
	for($i=0;$i<$n;$i++){
		$linea=$lineas[$l];
		$l++;
		$cadenas= explode(" ", $linea);
		$len=(int)$cadenas[0];
		$peces=array();
		for($j=0;$j<$len;$j++){
			array_push($peces,(int)$cadenas[$j+1]);
		}
		array_push($pescados,array('len' => $len, 'peces'=> $peces));
	}
	$aristas=array();
	for($i=0;$i<$m;$i++){
		$linea=$lineas[$l];
		$l++;
		$cadenas= explode(" ", $linea);
		array_push($aristas,array((int)$cadenas[0],(int)$cadenas[1],(int)$cadenas[2]));
	}
	$myjson=array('n' => $n, 'm' => $m, 'k' => $k, 'pescados' => $pescados, 'aristas' => $aristas);
	//var_dump($myjson);
	return $myjson;

}




//API Url
$url = 'http://localhost:8000/api/SynchronousShopping';
 
//Initiate cURL.
$ch = curl_init($url);
 

//Encode the array into JSON.
$jsonDataEncoded = json_encode(leer_data());
 
//Tell cURL that we want to send a POST request.
curl_setopt($ch, CURLOPT_POST, 1);
 
//Attach our encoded JSON string to the POST fields.
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
//Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//Execute the request
$result = curl_exec($ch);
echo "\n";
echo "\n";
if($result){
	$json=json_decode($result);
	if($json->exito==true){
		echo "el resultado es ",$json->resultado,"\n";
	}else{
		echo($result);
	}
}
echo "\n";
echo "\n";

curl_close($ch);