const fs = require('fs');
function str(n){
	return n.toString();
	
}

function pescado(len,peces){
	this.len=len;
	this.peces=peces;
}

function json(n,m,k,pescados,aristas){
	this.n=n;
	this.m=m;
	this.k=k;
	this.pescados=pescados;
	this.aristas=aristas;
}


function main(input) {
    //Enter your code here
	lineas= input.split("\r\n");
	cadena=lineas[0].split(" ");
	n=parseInt(cadena[0]);
	m=parseInt(cadena[1]);
	k=parseInt(cadena[2]);
	j=1;
	pescados=[];
	for(var i=0;i<n;i++){
		cadena=lineas[j];
		tam=parseInt(cadena[0])
		lista=(cadena.split(" "));
		peces=[];
		for(var l=0;l<tam;l++){
			peces.push(parseInt(lista[1+l]));
		}
		pes= new pescado(tam,peces);
		pescados.push(pes);
		j++;
	}
	aristas=[];
	for(var i=0;i<m;i++){
		cadena=lineas[j];
		lista=cadena.split(" ");
		aux=[parseInt(lista[0]),parseInt(lista[1]),parseInt(lista[2])];
		aristas.push(aux);
		j++;
	}
	
	var objeto= new json(n,m,k,pescados,aristas);
	myJSON = JSON.stringify(objeto);
	console.log(myJSON);
	
	
} 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   main(_input);
});