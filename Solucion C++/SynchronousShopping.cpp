#include<bits/stdc++.h>
using namespace std;
#define INF 0x3f3f3f3f
#define DINF 1e10
#define EPS 1e-15
#define PII acos(-1)
#define LL long long
#define Pii pair<int,int>
#define For(i,n) for(int i=0;i<n;i++)
#define ileer(n) scanf("%d",&n)
#define LLleer(n) scanf("%lld",&n)
#define i2leer(n,m) scanf("%d %d",&n,&m)
#define fleer(n) scanf("%lf",&n)
#define f2leer(n,m) scanf("%Lf %Lf",&n,&m)
#define MK make_pair
#define PB push_back
#define llenar(arr,val) memset(arr,val,sizeof(arr))
#define VLL vector< LL >
#define matrix vector<VI >
#define F first
#define S second
#define MAXN 1001
#define LOG 21
int n,m,k;
int pes[MAXN];

class nodo{
	public:
	int des,peso,mask,paso,resu;

	nodo(){}
	nodo(int a,int b){
		des=a;
		peso=b;
	}
	nodo(int a,int b, int c){
		des=a;
		peso=b;
		mask=c;
		
	}

	
	bool operator < (const nodo & ot)const{
		return peso>ot.peso;
	}
};

vector<nodo> grafo[MAXN];

int matriz[MAXN][1<<10];
vector<Pii > soluciones;
int disjktra(){
	for(int i=0;i<n;i++){
		for(int l=0;l<(1<<k);l++){
			matriz[i][l]=INF;
			matriz[i][l]=INF;
		}
	}
	soluciones.clear();
	priority_queue<nodo> cola;
	matriz[0][pes[0]]=0;
	cola.push(nodo(0,0,pes[0]));
	while(!cola.empty()){
		nodo actual=cola.top(); cola.pop();
		int u=actual.des;
		int peso=actual.peso;
		int mask=actual.mask;
		if(u==n-1){
			soluciones.PB(MK(mask,peso));
		}
		for(int i=0;i<grafo[u].size();i++){
			int v=grafo[u][i].des;
			int suma=grafo[u][i].peso;
			if(matriz[u][mask]+suma<matriz[v][mask|pes[v]]){
				matriz[v][mask|pes[v]]=matriz[u][mask]+suma;
				cola.push(nodo(v,matriz[u][mask]+suma,mask|pes[v]));
			}
		}
	
	}
	return 1;
	
}

int main(){
	i2leer(n,m); ileer(k);
	For(i,n){
		int t;
		ileer(t);
		pes[i]=0;
		For(j,t){
			int a;
			ileer(a);
			a--;
			pes[i]|=(1<<a);
		}
	}	
	For(i,m){
		int a,b,c;
		i2leer(a,b);
		a--;b--;
		ileer(c);
		grafo[a].PB(nodo(b,c));
		grafo[b].PB(nodo(a,c));
	}
	disjktra();
	int res=INF;
	for(int i=0;i<(1<<k)-1;i++){
		int mask=i;
		int rmask=(~mask)&((1<<k)-1);
		int v1=INF,v2=INF;
		for(int j=0;j<soluciones.size();j++){
			if((soluciones[j].F&mask)==mask) v1=min(v1,soluciones[j].S);
			if((soluciones[j].F&rmask)==rmask) v2=min(v2,soluciones[j].S);
		}
		if(v1==INF || v2==INF) continue;
		res=min(res,max(v1,v2));
	}
	printf("%d\n",res);
	
}