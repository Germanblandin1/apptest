<?php

class Error extends \SplEnum {
    const __default = self::estado_0;
    
    const estado_0 = 0;
    const estado_1 = 1;
    const estado_2 = 2;
    const estado_3 = 3;
}


function post_confirm() {
	$id = Input::get('service_id');
	$servicio = Service::find($id);
	$codigo_error=0;
	if($servicio != NULL){
		if($servicio->status_id == '6'){
			$codigo_error=new Error(Error::estado_2);
		}else{
			if($servicio->driver_id == NULL && $servicio->status_id == '1'){
				$driver_id = Input::get('driver_id');
				$servicio = Service::update($id, array(
					'driver_id' => $driver_id,
					'status_id' => '2',

				));
				Drive::update($driver_id,array(
					'available' => '0',

				));
				$driverTmp = Driver::find($driver_id);
				Service::update($id,array(
					'car_id' => $driverTmp->car_id,

				));
				//Notificar a usuario
				$pushMessage= 'Tu servicio ha sido confirmado!';

				$servicio = Service::find($id);
				$push = Push::make();
				if(!($servicio->user->uuid == '')){ 
					if($servicio->user->type == '1'){ // iphone 
						$result= $push->ios($servicio->user->uuid,$pushMessage,1,'honk.wav','Open',array('serviceId' => $servicio->id));
					}else{
						$result= $push->android2($servicio->user->uuid,$pushMessage,1,'default','Open',array('serviceId' => $servicio->id));
					}
				}
				new Error(Error::estado_0);
			}else{
				$codigo_error=new Error(Error::estado_1);
			}
		}
	}else{
		$codigo_error=new Error(Error::estado_3);
	}
	return Response::json(array('error' => $codigo_error));

}