<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


######################## Logica de aplicacion ############################################
class PriorityQueue extends \SplPriorityQueue{
    public function compare($p1, $p2) {
        if ($p1 == $p2) return 0;
        return ($p1 < $p2) ? 1 : -1;
   }
}

function node($des,$peso,$mask){
	return ['des' => $des,'peso' => $peso, 'mask' => $mask ];
}

function arista($des,$peso){
	return ['des' => $des,'peso' => $peso ];
}

class SynchronousShopping{
	 // Declaración de una propiedad
	private $INF; 
    private $n;
    private $m;
    private $k;
    private $pes;
	private $grafo;
	private $soluciones;
	
	
	private function procesar_data($data){
		$this->n=$data->n;
        $this->m=$data->m;
        $this->k=$data->k;
        $this->pes=new \SplFixedArray($this->n);
		
		$this->grafo=new \SplFixedArray($this->n);
        for($i=0;$i<$this->n;$i++){
			$len=$data->pescados[$i]->len;
			$this->pes[$i]=0;
			for($j=0;$j<$len;$j++){
				$a=$data->pescados[$i]->peces[$j]-1;
				$this->pes[$i]=$this->pes[$i] | ((1 << $a));
			}
			$this->grafo[$i]=new \SplDoublyLinkedList();
        }
		
		for($i=0;$i<$this->m;$i++){
			$a=$data->aristas[$i][0]-1;
			$b=$data->aristas[$i][1]-1;
			$c=$data->aristas[$i][2];
			$this->grafo[$a]->push(arista($b,$c));
			$this->grafo[$b]->push(arista($a,$c));
		}
		$this->INF=1000000000;
	}

	private function validar_data($data){
		if(!isset($data->n,$data->m,$data->k,$data->pescados,$data->aristas))
			return -1;
		if(count($data->pescados)!=$data->n) return -2;
		for($i=0;$i<$data->n;$i++){
			if(!isset($data->pescados[$i]->len,$data->pescados[$i]->peces)) return -3;
			if(count($data->pescados[$i]->peces)!=$data->pescados[$i]->len) return -4;
			for($j=0;$j<$data->pescados[$i]->len;$j++){
				if($data->pescados[$i]->peces[$j]<=0 || $data->pescados[$i]->peces[$j]>$data->k) return -5;
			}
		}
		if(count($data->aristas)!=$data->m) return -6;
		for($i=0;$i<$data->m;$i++){
			if(count($data->aristas[$i])!=3) return -7;
			if($data->aristas[$i][0]<=0 || $data->aristas[$i][0]>$data->n) return -8;
			if($data->aristas[$i][1]<=0 || $data->aristas[$i][1]>$data->n) return -9;
			if($data->aristas[$i][2]<=0 || $data->aristas[$i][2]>10000) return -10;
		}
		return 1;
	}
	
	
	private function disjktra(){
		$inicio=0;
		$n=$this->n;
		$k=$this->k;
		$final=$n-1;
		$matriz=new \SplFixedArray($n);
		for($i=0;$i<$n;$i++){
			$matriz[$i]=new \SplFixedArray(((1<<$k)));
			for($j=0;$j<((1<<$k));$j++){
				$matriz[$i][$j]=$this->INF;
			}
		}
		$this->soluciones=new \SplDoublyLinkedList();
		$cola=new PriorityQueue();
		$matriz[$inicio][$this->pes[$inicio]]=0;
		$cola->insert(node($inicio,0,$this->pes[$inicio]),0);

		while(!$cola->isEmpty()){
			$actual=$cola->top();$cola->next();
			$u=$actual['des'];
			$peso=$actual['peso'];
			$mask=$actual['mask'];
			if($u==$final){
				$this->soluciones->push([$mask,$peso]);
			}
			foreach($this->grafo[$u] as $arista){
				$v=$arista['des'];
				$suma=$arista['peso'];
				$nval=$matriz[$u][$mask]+$suma;
				if($nval<$matriz[$v][$mask|($this->pes[$v])]){
					$matriz[$v][$mask|$this->pes[$v]]=$nval;
					$cola->insert(node($v,$nval,$mask|($this->pes[$v])),$nval);
				}
			}
		}
	}

	private function solve(){
		$this->disjktra();
		$n=$this->n;
		$k=$this->k;
		$res=$this->INF;
		for($i=0;$i<((1<<$k));$i++){
			$mask=$i;
			$rmask=(~$mask)&((1<<$k)-1);
			$v1=$this->INF;
			$v2=$this->INF;
			foreach($this->soluciones as $sol){
				if(($sol[0]&$mask)==$mask){ $v1=min($v1,$sol[1]);}
				if(($sol[0]&$rmask)==$rmask) {$v2=min($v2,$sol[1]);}
			}
			if($v1==$this->INF || $v2==$this->INF) continue;
			$res=min($res,max($v1,$v2));
		}
		return $res;
	}
	
	
	
    // Declaración de un método
    public function iniciar($data) {
    	$valido=$this->validar_data($data);
    	if($valido>0){
    		$this->procesar_data($data);
			return $this->solve();
    	}
    	return $valido;
        
    }
}
######################## Fin de Logica de aplicacion ############################################




######################## Controlador ############################################
class ShoppingController extends Controller
{	
	private function manejoDeError($res){
		$message="error";
		switch ($res) {
    		case -1:
        		$message="No existen los parametros correctos en el JSON.";
        		break;
   			case -2:
		        $message='El parametro "pescados" debe tener N elementos';
		        break;
		    case -3:
		        $message='El parametro "pescados" no contiene los parametros correctos';
		        break;
		    case -4:
		        $message='La cantidad de cada parametro "peces" debe coincidir con el parametro "len"';
		        break;
		    case -5:
        		$message='cada pescado en "peces" debe estar en el rango [1,k]';
        		break;
   			case -6:
		        $message='El parametro "aristas" debe tener M elementos';
		        break;
		    case -7:
		        $message='Cada elemento de "aristas" debe contener 3 valores';
		        break;
		    case -8:
		        $message='El primer valor de cada elemento "arista" debe estar en el rango [1,N]';
		        break;
		    case -9:
		        $message='El segundo valor de cada elemento "arista" debe estar en el rango [1,N]';
		        break;
		    case -10:
		        $message='El tercer valor de cada elemento "arista" debe estar en el rango [1,10000;]';
		        break;

		}
		return $message;
	}

	public function shoppingHelp(){
		$message='
			Bienvenido a la ayuda

			Primero que nada puede consultar el problema en la siguiente URL:
				https://www.hackerrank.com/challenges/synchronous-shopping
			Para obtener la solucion al problema se debe hacer la siguiente solicitud:
				POST /api/SynchronousShopping HTTP/1.1
				Content-Type: application/json
				Dicha solicitud debe recibir un JSON.
				Este JSON debe seguir el siguiente formato:


			{
				"n":<valor de n>,
				"m":<valor de m,
				"k":<valor de k>
				"pescados": <lista de tamaño n con elementos del siguiente formato>
					{
						"len": <cantidad de peces>,
						"peces": <lista de tamaño "len" que indica los peces en el nodo i>
					}
				"aristas": <lista de tamaño m con elementos del siguiente formato>
					<listas con 3 elementos indican los nodos de cada arista y el peso correspondiente>
			}

				ejemplo: {"n":5,"m":5,"k":5,"pescados":[{"len":1,"peces":[1]},{"len":1,"peces":[2]},{"len":1,"peces":[3]},{"len":1,"peces":[4]},{"len":1,"peces":[5]}],"aristas":[[1,2,10],[1,3,10],[2,4,10],[3,5,10],[4,5,10]]}

				Cabe destacar que los valores incluidos en el JSON deben cumplir con todas las condiciones descritas en el problema.
		';
    	return \Response::json(['exito' => true,'Mensaje' => $message],200);
    }
    public function shoppingGet(){
    	$message='
    		Bienvenidos a la solucion SynchronousShopping

    		Para solicitar la solucion al problema solicite:
    			POST /api/SynchronousShopping/help HTTP/1.1
    			Content-Type: application/json
    			Debe contener un JSON con el formato adecuado
    			Para mas informacion consulte:
    			Consulte: GET /api/SynchronousShopping/help HTTP/1.1
    	';
    	return \Response::json(['exito' => true,'Mensaje' => $message],200);
    }
    public function shoppingPost(Request $request){
    	$res=-1;
    	$exito=false;
    	$consulta=' Consulte: GET /api/SynchronousShopping/help HTTP/1.1';
    	$message="errorfffffff";
    	$status=200;

    	if (!($request->headers->get('Content-Type')=='application/json') || !is_array($request->all())) {
            $message='Formato invalido, No se recibieron parametros'.$consulta;
        }else{
	        try {
		        $data=json_decode($request->getContent());

		        $instancia=new SynchronousShopping();
		        $res=$instancia->iniciar($data);

		        if($res<0){
		            $message=($this->manejoDeError($res)).$consulta;
		        }else{
		            $message="NO";
		            $exito=true;
		        }
	        } catch (Exception $e) {
	            $status=500;
	            $message="Error Interno vuelva a intentar";
	        }

        }
        
        return \Response::json(['exito' => $exito, 'resultado' => $res, 'error' => $message],$status);
    }
}
######################## Fin controlador ############################################